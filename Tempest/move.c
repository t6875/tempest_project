#include "move.h"

void move_object_on_line(PointDouble *object, PointDouble *target, double speed) {
    double angle = atan2(target->y - object->y, target->x - object->x);
    double cosinus = cos(angle) * speed;
    double sinus = sin(angle) * speed;

    object->x += cosinus;
    object->y += sinus;
}

void move_projectiles(arraylist *projectiles) {
    for (int i = 0; i < projectiles->size; i++) {
        Projectile *projectile = arraylist_get(projectiles, i);
        move_object_on_line(&projectile->pos, &projectile->target, PROJECTILE_PER_FRAME_SPEED);
    }
}

void move_enemies(arraylist *enemies, float enemyPerFrameSpeed) {
    for (int i = 0; i < enemies->size; i++) {
        Enemy *enemy = arraylist_get(enemies, i);
        move_object_on_line(&enemy->pos, &enemy->target, enemyPerFrameSpeed);
    }
}
