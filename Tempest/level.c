#include "level.h"

void TriangleLevel(SDL_Window* window, Polygon *triangle, Polygon *mini_triangle){
    generate_triangle(mini_triangle, 80, window);
    generate_triangle(triangle, 400, window);
}

void SquareLevel(SDL_Window* window, Polygon *square, Polygon *mini_square){
    generate_square(mini_square, 80, window);
    generate_square(square, 400, window);
}

void LosangeLevel(SDL_Window* window, Polygon *losange, Polygon *mini_losange){
    generate_losange(mini_losange, 45, window);
    generate_losange(losange, 270, window);
}

void PentagoneLevel(SDL_Window* window, Polygon *pentagone, Polygon *mini_pentagone){
    generate_pentagone(mini_pentagone, 60, window);
    generate_pentagone(pentagone, 300, window);
}

void HexagoneLevel(SDL_Window* window, Polygon *hexagone, Polygon *mini_hexagone){
    generate_hexagone(mini_hexagone, 50, window);
    generate_hexagone(hexagone, 250, window);
}

void HeartLevel(SDL_Window* window, Polygon *heart, Polygon *mini_heart){
    generate_heart(mini_heart, 35, window);
    generate_heart(heart, 175, window);
}

void CrossLevel(SDL_Window* window, Polygon *cross, Polygon *mini_cross){
    generate_cross(mini_cross, 40, window);
    generate_cross(cross, 160, window);
}

int RandomLevel(SDL_Window* window, Polygon *big, Polygon *small, int previousValue){
    int val = rand() % NB_LEVEL;
    while (val == previousValue){
        val = rand() % NB_LEVEL;
    }
    switch(val){
        case 0:
            TriangleLevel(window, big, small);
            break;
        case 1:
            SquareLevel(window, big, small);
            break;
        case 2:
            LosangeLevel(window, big, small);
            break;
        case 3:
            PentagoneLevel(window, big, small);
            break;
        case 4:
            HexagoneLevel(window, big, small);
            break;
        case 5:
            HeartLevel(window, big, small);
            break;
        case 6:
            CrossLevel(window, big, small);
            break;
        default:
            break;
    }
    return val;
}