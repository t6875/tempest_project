#ifndef TEST_SDL_MOVE_H
#define TEST_SDL_MOVE_H
#include "game.h"

void move_object_on_line(PointDouble *object, PointDouble *target, double speed);
void move_projectiles(arraylist *projectiles);
void move_enemies(arraylist *enemies, float enemyPerFrameSpeed);

#endif //TEST_SDL_MOVE_H
