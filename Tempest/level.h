#ifndef TEST_SDL_LEVEL_H
#define TEST_SDL_LEVEL_H
#include <SDL.h>
#include "polygons.h"
#include "render.h"
#define NB_LEVEL 7;

SDL_Point get_intermediate_point(Polygon *poly, SDL_Point offset, int sides_index, int points_index);
void generate_linking_lines(Polygon *big, Polygon *lil);
void TriangleLevel(SDL_Window* window, Polygon *triangle, Polygon *mini_triangle);
void SquareLevel(SDL_Window* window, Polygon *square, Polygon *mini_square);
void LosangeLevel(SDL_Window* window, Polygon *losange, Polygon *mini_losange);
void PentagoneLevel(SDL_Window* window, Polygon *pentagone, Polygon *mini_pentagone);
void HexagoneLevel(SDL_Window* window, Polygon *hexagone, Polygon *mini_hexagone);
void HeartLevel(SDL_Window* window, Polygon *heart, Polygon *mini_heart);
void CrossLevel(SDL_Window* window, Polygon *cross, Polygon *mini_cross);
int RandomLevel(SDL_Window* window, Polygon *big, Polygon *small, int previousValue);

#endif //TEST_SDL_LEVEL_H
