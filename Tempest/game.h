#ifndef TEST_SDL_GAME_H
#define TEST_SDL_GAME_H
#define ENEMY_SPEED_DIFFICULTY_INCREASE 0.05
#define ENEMY_SIZE 15
#define PROJECTILE_PER_FRAME_SPEED 1
#define PROJECTILE_SIZE 10
#define ENEMY_GENERATION_SPEED_INCREASE 250
#define MIN_ENEMY_SPAWN_DELAY 500

#include <SDL.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include "arraylist.h"
#include "polygons.h"

typedef struct PointDouble {
    double x, y;
} PointDouble;

typedef struct Projectile {
    PointDouble pos;
    PointDouble target;
} Projectile;

typedef struct Enemy {
    PointDouble pos;
    PointDouble target;
} Enemy;

void handle_movement_event(SDL_Keycode keycode, Polygon *polygon, Field *selected_field);
void handle_shoot_event(SDL_Keycode keycode, int field_index, Polygon *lil, Polygon *big, arraylist *projectiles);
void pick_random_field(Polygon *polygon, Field *random_field);
void spawn_enemies(arraylist *enemies, Uint32 *next_enemy_spawn_time, Polygon *big, Polygon *lil, float enemySpawnDelay);
void check_score_and_change_level(int *score, int *scoreToBeat, arraylist *enemies, arraylist *projectiles, SDL_Renderer *renderer, int *previousLevelNumber,SDL_Window *window, Polygon *big_polygon, Polygon *mini_polygon, Field *selected_field, float *enemyPerFrameSpeed, float *enemySpawnDelay);
void check_lives_and_handle_refresh(SDL_Renderer *renderer, SDL_Window *window, int nbLives, bool hasClear, int displayedScore, Polygon *big_polygon, Polygon *mini_polygon,
                                    Field *selected_field, arraylist *projectiles, arraylist *enemies);

#endif //TEST_SDL_GAME_H
