#include "render.h"

const SDL_Color base_color = {255, 255, 0, 255};
const SDL_Color red_color = {255, 0, 0, 255};
const SDL_Color green_color = {0,255,0,255};
const SDL_Color blue_color = {0, 0, 255, 255};
const SDL_Color white_color = {255,255,255,255};

void draw_polygon(SDL_Renderer* renderer, Polygon *polygon)
{
    SDL_RenderDrawLines(renderer, polygon->coords, polygon->sidesCount + 1);
}

void draw_polygons(Polygon *big, Polygon *small, SDL_Renderer* renderer){
    draw_polygon(renderer, big);
    draw_polygon(renderer, small);
}

void draw_rect(SDL_Renderer *renderer, int width, int height, int x, int y){
    SDL_Rect rect =  {x-(width/2), y-(height/2), width,height};
    SDL_RenderFillRect(renderer, &rect);
}

void render_projectiles(SDL_Renderer *renderer, arraylist *projectiles) {
    for (int i = 0; i < projectiles->size; i++) {
        Projectile *projectile = arraylist_get(projectiles, i);
        draw_rect(renderer, PROJECTILE_SIZE, PROJECTILE_SIZE, (int) projectile->pos.x, (int) projectile->pos.y);
    }
}

void render_enemies(SDL_Renderer *renderer, arraylist *enemies) {
    for (int i = 0; i < enemies->size; i++) {
        Enemy *enemy = arraylist_get(enemies, i);
        draw_rect(renderer, ENEMY_SIZE, ENEMY_SIZE, (int) enemy->pos.x, (int) enemy->pos.y);
    }
}

void render_linking_lines(SDL_Renderer *renderer,  Polygon *big, Polygon *lil){
    for (int i = 0; i < big->sidesCount; i++) {
        for (int j = 0; j < big->pointsPerSideCount; j++) {
            SDL_RenderDrawLine(
                    renderer,
                    big->intermediate_points[i*big->pointsPerSideCount + j].x, big->intermediate_points[i * big->pointsPerSideCount + j].y,
                    lil->intermediate_points[i*big->pointsPerSideCount + j].x, lil->intermediate_points[i * big->pointsPerSideCount + j].y
            );
        }
    }
}

void draw_selected_field(SDL_Renderer *renderer, Field *field) {
    SDL_SetRenderDrawColor(renderer, red_color.r, red_color.g, red_color.b, red_color.a);
    SDL_RenderDrawLine(renderer, field->start.x, field->start.y, field->end.x, field->end.y);
}

void handle_screen_refresh(SDL_Renderer *renderer, Polygon *big, Polygon *lil, Field *selected_field, arraylist *projectiles, arraylist *enemies) {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // Draw background
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, base_color.r, base_color.g, base_color.b, base_color.a);
    draw_polygons(big, lil, renderer);
    render_linking_lines(renderer, big, lil);
    draw_selected_field(renderer, selected_field);

    SDL_SetRenderDrawColor(renderer, blue_color.r, blue_color.g, blue_color.b, blue_color.a);
    render_projectiles(renderer, projectiles);
    SDL_SetRenderDrawColor(renderer, green_color.r, green_color.g, green_color.b, green_color.a);
    render_enemies(renderer, enemies);
}


