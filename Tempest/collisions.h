#ifndef TEST_SDL_COLLISIONS_H
#define TEST_SDL_COLLISIONS_H
#include "game.h"

void check_projectiles_collisions(arraylist *projectiles, arraylist *enemies, int *score, int *displayed_score);
void check_enemies_collisions(arraylist *enemies, int *nbLives);

#endif //TEST_SDL_COLLISIONS_H
