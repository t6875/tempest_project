#include "collisions.h"

void check_projectiles_collisions(arraylist *projectiles, arraylist *enemies, int *score, int *displayed_score) {
    for (int i = 0; i < projectiles->size; i++) {
        Projectile *projectile = arraylist_get(projectiles, i);
        SDL_Rect projectile_rect = {(int) projectile->pos.x, (int) projectile->pos.y, PROJECTILE_SIZE, PROJECTILE_SIZE};

        //Check enemy collision
        for (int j = 0; j < enemies->size; j++) {
            Enemy *enemy = arraylist_get(enemies, j);
            SDL_Rect enemy_rect = {(int) enemy->pos.x, (int) enemy->pos.y, ENEMY_SIZE, ENEMY_SIZE};
            if (SDL_HasIntersection(&projectile_rect, &enemy_rect)) {
                arraylist_remove(enemies, j);
                arraylist_remove(projectiles, i);
                *score = *score+1;
                *displayed_score = *displayed_score+1;
                break;
            }
        }

        //Check field collision
        SDL_Point target_point = {(int) projectile->target.x, (int) projectile->target.y};
        if (SDL_PointInRect(&target_point, &projectile_rect)) {
            arraylist_remove(projectiles, i);
        }
    }
}

void check_enemies_collisions(arraylist *enemies, int *nbLives) {
    for (int i = 0; i < enemies->size; i++) {
        Enemy *enemy = arraylist_get(enemies, i);
        SDL_Rect enemy_rect = {(int) enemy->pos.x, (int) enemy->pos.y, ENEMY_SIZE, ENEMY_SIZE};
        SDL_Point enemy_target_point = {(int) enemy->target.x, (int) enemy->target.y};
        if (SDL_PointInRect(&enemy_target_point, &enemy_rect)) {
            arraylist_remove(enemies, i);
            *nbLives = *nbLives-1;
        }
    }
}
