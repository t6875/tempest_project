#ifndef TEST_SDL_TEXT_H
#define TEST_SDL_TEXT_H
#define RESULT_TEXT_SIZE 100
#define SMALL_TEXT_SIZE 10

#include "SDL_render.h"
#include <string.h>

void displayText(SDL_Renderer* renderer, char* str, int x, int y, int size, SDL_Color color);
void compute_score_text(SDL_Renderer *renderer, int actualScore);
void compute_lives_text(SDL_Renderer *renderer, int nbLives);

#endif //TEST_SDL_TEXT_H
