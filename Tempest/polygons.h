#ifndef TEST_SDL_POLYGONS_H
#define TEST_SDL_POLYGONS_H
#define MAX_POLYGON_SIDES_COUNT 25
#include <SDL.h>

typedef struct Polygon {
    SDL_Point *coords;
    SDL_Point *intermediate_points;
    int sidesCount;
    int pointsPerSideCount;
} Polygon;

typedef struct Field {
    Uint8 index;
    SDL_Point start;
    SDL_Point end;
} Field;

Polygon* polygon_create();
int get_field_count(Polygon *polygon);
void generate_linking_lines(Polygon *big, Polygon *lil);
SDL_Point get_intermediate_point(Polygon *poly, SDL_Point offset, int sides_index, int points_index);
void generate_triangle(Polygon *polygon, int size, SDL_Window* window);
void generate_square(Polygon *polygon, int size, SDL_Window *window);
void generate_losange(Polygon *polygon, int size, SDL_Window *window);
void generate_pentagone(Polygon *polygon, int size, SDL_Window *window);
void generate_hexagone(Polygon *polygon, int size, SDL_Window *window);
void generate_heart(Polygon *polygon, int size, SDL_Window *window);
void generate_cross(Polygon *polygon, int size, SDL_Window *window);

#endif //TEST_SDL_POLYGONS_H
