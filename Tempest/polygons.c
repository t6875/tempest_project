#include "polygons.h"

Polygon* polygon_create() {
    Polygon* polygon = malloc(sizeof(Polygon));
    polygon->coords = malloc(sizeof(SDL_Point) * MAX_POLYGON_SIDES_COUNT);
    return polygon;
}

int get_field_count(Polygon *polygon){
    return polygon->sidesCount * polygon->pointsPerSideCount;
}

void generate_linking_lines(Polygon *big, Polygon *lil) {
    big->intermediate_points = malloc(sizeof(SDL_Point) * big->sidesCount * big->pointsPerSideCount);
    lil->intermediate_points = malloc(sizeof(SDL_Point) * big->sidesCount * big->pointsPerSideCount);
    for (int i = 0; i < big->sidesCount; i++) {
        SDL_Point big_offset = {(big->coords[i+1].x - big->coords[i].x) / big->pointsPerSideCount,
                                (big->coords[i+1].y - big->coords[i].y) / big->pointsPerSideCount};
        SDL_Point lil_offset = {(lil->coords[i+1].x - lil->coords[i].x) / big->pointsPerSideCount,
                                (lil->coords[i+1].y - lil->coords[i].y) / big->pointsPerSideCount};

        for (int j = 0; j < big->pointsPerSideCount; j++) {
            big->intermediate_points[i*big->pointsPerSideCount + j] = get_intermediate_point(big, big_offset, i, j);
            lil->intermediate_points[i*big->pointsPerSideCount + j] = get_intermediate_point(lil, lil_offset, i, j);
        }
    }
}

SDL_Point get_intermediate_point(Polygon *poly, SDL_Point offset, int sides_index, int points_index) {
    return (SDL_Point) {
            poly->coords[sides_index].x + offset.x * points_index,
            poly->coords[sides_index].y + offset.y * points_index
    };
}

void generate_triangle(Polygon *polygon, int size, SDL_Window* window){
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    int width_origin = w / 2;
    int height_origin = h / 2 - size / 2;

    polygon->coords[0].x = width_origin;
    polygon->coords[0].y = height_origin;
    polygon->coords[1].x = width_origin + size;
    polygon->coords[1].y = height_origin + size;
    polygon->coords[2].x = width_origin - size;
    polygon->coords[2].y = height_origin + size;
    polygon->coords[3].x = width_origin;
    polygon->coords[3].y = height_origin;

    polygon->sidesCount = 3;
    polygon->pointsPerSideCount = 3;
}
void generate_square(Polygon *polygon, int size, SDL_Window *window){
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    int width_origin = w/2 - size/2;
    int height_origin = h/2 - size/2;

    polygon->coords[0].x = width_origin;
    polygon->coords[0].y = height_origin;
    polygon->coords[1].x = width_origin + size;
    polygon->coords[1].y = height_origin;
    polygon->coords[2].x = width_origin + size;
    polygon->coords[2].y = height_origin + size;
    polygon->coords[3].x = width_origin;
    polygon->coords[3].y = height_origin + size;
    polygon->coords[4].x = width_origin;
    polygon->coords[4].y = height_origin;

    polygon->sidesCount = 4;
    polygon->pointsPerSideCount = 3;
}

void generate_losange(Polygon *polygon, int size, SDL_Window *window){
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    int width_origin = w/2;
    int height_origin = h/2 - size;

    polygon->coords[0].x = width_origin;
    polygon->coords[0].y = height_origin;
    polygon->coords[1].x = width_origin + size/2;
    polygon->coords[1].y = height_origin+size;
    polygon->coords[2].x = width_origin;
    polygon->coords[2].y = height_origin + (2*size);
    polygon->coords[3].x = width_origin - size/2;
    polygon->coords[3].y = height_origin + size;
    polygon->coords[4].x = width_origin;
    polygon->coords[4].y = height_origin;

    polygon->sidesCount = 4;
    polygon->pointsPerSideCount = 3;
}

void generate_pentagone(Polygon *polygon, int size, SDL_Window *window){
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    int width_origin = w/2;
    int height_origin = h/2 - 4*size/5;

    polygon->coords[0].x = width_origin;
    polygon->coords[0].y = height_origin;
    polygon->coords[1].x = width_origin + 3*size/4;
    polygon->coords[1].y = height_origin + size;
    polygon->coords[2].x = width_origin + size/4;
    polygon->coords[2].y = height_origin + size*1.5;
    polygon->coords[3].x = width_origin - size/4;
    polygon->coords[3].y = height_origin + size*1.5;
    polygon->coords[4].x = width_origin-3*size/4;
    polygon->coords[4].y = height_origin+size;
    polygon->coords[5].x = width_origin;
    polygon->coords[5].y = height_origin;

    polygon->sidesCount = 5;
    polygon->pointsPerSideCount = 3;
}

void generate_hexagone(Polygon *polygon, int size, SDL_Window *window){
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    int width_origin = w/2;
    int height_origin = h/2 - size;

    polygon->coords[0].x = width_origin;
    polygon->coords[0].y = height_origin;
    polygon->coords[1].x = width_origin + size;
    polygon->coords[1].y = height_origin + size/2;
    polygon->coords[2].x = width_origin + size;
    polygon->coords[2].y = height_origin + size*1.5;
    polygon->coords[3].x = width_origin ;
    polygon->coords[3].y = height_origin + size*2;
    polygon->coords[4].x = width_origin-size;
    polygon->coords[4].y = height_origin+size*1.5;
    polygon->coords[5].x = width_origin-size;
    polygon->coords[5].y = height_origin+size/2;
    polygon->coords[6].x = width_origin;
    polygon->coords[6].y = height_origin;

    polygon->sidesCount = 6;
    polygon->pointsPerSideCount = 2;
}

void generate_heart(Polygon *polygon, int size, SDL_Window *window){
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    int width_origin = w/2;
    int height_origin = h/2-size/3;

    polygon->coords[0].x = width_origin;
    polygon->coords[0].y = height_origin-size/4;
    polygon->coords[1].x = width_origin+2*size/3;
    polygon->coords[1].y = height_origin-3*size/4;
    polygon->coords[2].x = width_origin+(size*1.5);
    polygon->coords[2].y = height_origin-3*size/4;
    polygon->coords[3].x = width_origin+(size*2);
    polygon->coords[3].y = height_origin;
    polygon->coords[4].x = width_origin;
    polygon->coords[4].y = height_origin+(size*1.5);
    polygon->coords[5].x = width_origin-(size*2);
    polygon->coords[5].y = height_origin;
    polygon->coords[6].x = width_origin-(size*1.5);
    polygon->coords[6].y = height_origin-3*size/4;
    polygon->coords[7].x = width_origin-2*size/3;
    polygon->coords[7].y = height_origin-3*size/4;
    polygon->coords[8].x = width_origin;
    polygon->coords[8].y = height_origin-size/4;

    polygon->sidesCount = 8;
    polygon->pointsPerSideCount = 2;
}

void generate_cross(Polygon *polygon, int size, SDL_Window *window){
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    int width_origin = w/2 - size/2;
    int height_origin = h/2 -(size*1.5);

    polygon->coords[0].x = width_origin;
    polygon->coords[0].y = height_origin;
    polygon->coords[1].x = width_origin + size;
    polygon->coords[1].y = height_origin;
    polygon->coords[2].x = width_origin + size;
    polygon->coords[2].y = height_origin + size;
    polygon->coords[3].x = width_origin +(size*2) ;
    polygon->coords[3].y = height_origin + size;
    polygon->coords[4].x = width_origin+(size*2);
    polygon->coords[4].y = height_origin+(size*2);
    polygon->coords[5].x = width_origin+size;
    polygon->coords[5].y = height_origin+(size*2);
    polygon->coords[6].x = width_origin+size;
    polygon->coords[6].y = height_origin+(size*3);
    polygon->coords[7].x = width_origin;
    polygon->coords[7].y = height_origin + (size*3);
    polygon->coords[8].x = width_origin ;
    polygon->coords[8].y = height_origin + (size*2);
    polygon->coords[9].x = width_origin-size;
    polygon->coords[9].y = height_origin+(size*2);
    polygon->coords[10].x = width_origin-size;
    polygon->coords[10].y = height_origin+size;
    polygon->coords[11].x = width_origin;
    polygon->coords[11].y = height_origin+size;
    polygon->coords[12].x = width_origin;
    polygon->coords[12].y = height_origin;

    polygon->sidesCount = 12;
    polygon->pointsPerSideCount = 2;
}