#ifndef TEST_SDL_RENDER_H
#define TEST_SDL_RENDER_H

#include "game.h"

extern const SDL_Color base_color;
extern const SDL_Color red_color;
extern const SDL_Color green_color;
extern const SDL_Color blue_color;
extern const SDL_Color white_color;

void draw_polygon(SDL_Renderer* renderer, Polygon *polygon);
void draw_polygons(Polygon *big, Polygon *small, SDL_Renderer* renderer);
void draw_rect(SDL_Renderer *renderer, int width, int height, int x, int y);
void render_projectiles(SDL_Renderer *renderer, arraylist *projectiles);
void render_enemies(SDL_Renderer *renderer, arraylist *enemies);
void render_linking_lines(SDL_Renderer *renderer,  Polygon *big, Polygon *lil);
void draw_selected_field(SDL_Renderer *renderer, Field *field);
void handle_screen_refresh(SDL_Renderer *renderer, Polygon *big, Polygon *lil, Field *selected_field, arraylist *projectiles, arraylist *enemies);

#endif //TEST_SDL_RENDER_H
