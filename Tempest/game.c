#include "game.h"
#include "level.h"
#include "render.h"
#include "collisions.h"
#include "move.h"
#include "text.h"

int main(int argc, char** argv)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        fprintf(stderr,"Pb init SDL\n");
        return 0;
    }

    SDL_Window* window = SDL_CreateWindow("Test_SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600,
                                          SDL_WINDOW_SHOWN|SDL_WINDOW_ALLOW_HIGHDPI);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
    assert (renderer != NULL);
    srand(time(NULL));
    bool quit = false;

    //setup variables
    Polygon *big_polygon = polygon_create();
    Polygon *mini_polygon = polygon_create();
    Field *selected_field = malloc(sizeof(Field));
    arraylist *projectiles = arraylist_create();
    arraylist *enemies = arraylist_create();
    float enemySpawnDelay = 2500;
    Uint32 next_enemy_spawn_time =  SDL_GetTicks() + enemySpawnDelay;
    int nbLives = 3;
    int score = 0;
    int scoreToBeat = 6;
    int displayedScore = 0;
    int previousLevelNumber =-1;
    float enemyPerFrameSpeed = 0.4;
    bool hasClear = false;

    previousLevelNumber = RandomLevel(window, big_polygon, mini_polygon, previousLevelNumber);
    generate_linking_lines(big_polygon, mini_polygon);

    selected_field->index = 0;
    selected_field->start = big_polygon->intermediate_points[0];
    selected_field->end = big_polygon->intermediate_points[1];

    while (!quit)
    {
        // Event polling
        SDL_Event event;
        while (!quit && SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_KEYDOWN:
                    handle_movement_event(event.key.keysym.sym, big_polygon, selected_field);
                    handle_shoot_event(event.key.keysym.sym, selected_field->index, mini_polygon, big_polygon, projectiles);
                    break;
                case SDL_QUIT:
                    quit = true;
                    break;
            }
        }

        // Game logic
        spawn_enemies(enemies, &next_enemy_spawn_time, big_polygon, mini_polygon, enemySpawnDelay);
        move_projectiles(projectiles);
        move_enemies(enemies, enemyPerFrameSpeed);
        check_score_and_change_level(&score, &scoreToBeat, enemies, projectiles, renderer, &previousLevelNumber, window,
                                     big_polygon, mini_polygon, selected_field, &enemyPerFrameSpeed, &enemySpawnDelay);

        // Collision detection
        check_projectiles_collisions(projectiles, enemies, &score, &displayedScore);
        check_enemies_collisions(enemies, &nbLives);

        // Rendering
        check_lives_and_handle_refresh(renderer, window, nbLives, hasClear, displayedScore , big_polygon, mini_polygon,
                                       selected_field, projectiles, enemies);
    }
    //Free pointers
    free(big_polygon->coords);
    free(big_polygon->intermediate_points);
    free(big_polygon);
    free(mini_polygon->coords);
    free(mini_polygon->intermediate_points);
    free(mini_polygon);
    free(selected_field);
    arraylist_destroy(projectiles);
    arraylist_destroy(enemies);
    SDL_Quit();
    return 0;
}

void handle_movement_event(SDL_Keycode keycode, Polygon *polygon, Field *selected_field) {
    int field_index =  selected_field->index;
    switch (keycode) {
        case SDLK_LEFT:
            field_index--;
            break;
        case SDLK_RIGHT:
            field_index++;
            break;
        default:
            break;
    }
    if (field_index < 0) {
        field_index = get_field_count(polygon) - 1;
    }
    selected_field->start = polygon->intermediate_points[(field_index) % get_field_count(polygon)];
    selected_field->end = polygon->intermediate_points[(field_index+1) % get_field_count(polygon)];
    selected_field->index = field_index % get_field_count(polygon);
}

void handle_shoot_event(SDL_Keycode keycode, int field_index, Polygon *lil, Polygon *big, arraylist *projectiles) {
    if (keycode != SDLK_SPACE) {
        return;
    }
    Projectile *projectile = malloc(sizeof(Projectile));
    Uint8 endIndex = (field_index + 1) % get_field_count(big);
    projectile->pos = (PointDouble) {
            .x = (big->intermediate_points[field_index].x + big->intermediate_points[endIndex].x) / 2,
            .y = (big->intermediate_points[field_index].y + big->intermediate_points[endIndex].y) / 2
    };
    projectile->target = (PointDouble){
            .x = (lil->intermediate_points[field_index].x + lil->intermediate_points[endIndex].x) / 2,
            .y = (lil->intermediate_points[field_index].y + lil->intermediate_points[endIndex].y) / 2
    };
    arraylist_add(projectiles, projectile);
}

void pick_random_field(Polygon *polygon, Field *random_field) {
    int field_index = rand() % get_field_count(polygon);
    random_field->start = polygon->intermediate_points[field_index];
    random_field->end = polygon->intermediate_points[(field_index + 1) % get_field_count(polygon)];
    random_field->index = field_index;
}

void spawn_enemies(arraylist *enemies, Uint32 *next_enemy_spawn_time, Polygon *big, Polygon *lil, float enemySpawnDelay) {
    if (SDL_GetTicks() < *next_enemy_spawn_time) {
        return;
    }

    Field *field = malloc(sizeof(Field));
    pick_random_field(big, field);

    Enemy *enemy = malloc(sizeof(Enemy));
    enemy->pos = (PointDouble) {
            .x = (lil->intermediate_points[field->index].x + lil->intermediate_points[(field->index + 1) % get_field_count(lil)].x) / 2,
            .y = (lil->intermediate_points[field->index].y + lil->intermediate_points[(field->index + 1) % get_field_count(lil)].y) / 2
    };
    enemy->target = (PointDouble) {
            .x = (big->intermediate_points[field->index].x + big->intermediate_points[(field->index + 1) % get_field_count(big)].x) / 2,
            .y = (big->intermediate_points[field->index].y + big->intermediate_points[(field->index + 1) % get_field_count(big)].y) / 2
    };
    arraylist_add(enemies, enemy);
    *next_enemy_spawn_time = SDL_GetTicks() + enemySpawnDelay;
    free(field);
}

void check_score_and_change_level(int *score, int *scoreToBeat, arraylist *enemies, arraylist *projectiles, SDL_Renderer *renderer, int *previousLevelNumber,
                                  SDL_Window *window, Polygon *big_polygon, Polygon *mini_polygon, Field *selected_field, float *enemyPerFrameSpeed, float *enemySpawnDelay){
    if(*score % *scoreToBeat == *scoreToBeat - 1){
        arraylist_clear(enemies);
        arraylist_clear(projectiles);
        SDL_RenderClear(renderer);

        *previousLevelNumber = RandomLevel(window, big_polygon, mini_polygon, *previousLevelNumber);
        generate_linking_lines(big_polygon, mini_polygon);
        selected_field->index = 0;
        selected_field->start = big_polygon->intermediate_points[0];
        selected_field->end = big_polygon->intermediate_points[1];

        *score=0;
        *scoreToBeat+=2;
        *enemyPerFrameSpeed = *enemyPerFrameSpeed + ENEMY_SPEED_DIFFICULTY_INCREASE;
        if (*enemySpawnDelay >= MIN_ENEMY_SPAWN_DELAY){
            *enemySpawnDelay = *enemySpawnDelay - ENEMY_GENERATION_SPEED_INCREASE;
        }
    }
}

void check_lives_and_handle_refresh(SDL_Renderer *renderer, SDL_Window *window, int nbLives, bool hasClear, int displayedScore, Polygon *big_polygon, Polygon *mini_polygon,
                                    Field *selected_field, arraylist *projectiles, arraylist *enemies) {
    if(nbLives > 0){
        handle_screen_refresh(renderer, big_polygon, mini_polygon, selected_field, projectiles, enemies);
        compute_score_text(renderer, displayedScore);
        compute_lives_text(renderer, nbLives);
        SDL_RenderPresent(renderer);
    }
    if(nbLives <= 0){
        SDL_SetRenderDrawColor(renderer, 0,0,0,255);
        if(!hasClear){
            SDL_RenderClear(renderer);
            hasClear = true;
        }
        int w, h;
        SDL_GetWindowSize(window, &w, &h);
        displayText(renderer, "GAME OVER !", w/8-60, h/8+30, 3, red_color);
        SDL_RenderPresent(renderer);
    }
}